(async () => {
	const request = await fetch(`http://${location.hostname}:8080`);

	const IFRAME = document.createElement('iframe');
	document.body.querySelector('.frame').appendChild(IFRAME);

	const cnt = await request.text();
	const doc = IFRAME.contentDocument;

	IFRAME.contentWindow.addEventListener('click', (e) => {
		if (e.composedPath()) {
			e.preventDefault();
		}
	})

	doc.open();
	doc.write(cnt);
	doc.write('<style>html{scroll-behavior:smooth;}</style>');
	doc.close();

	let elem = doc.querySelector('h1');
	elem.click();

	const click = () => elem.click();

	let end = 'top';
	const scroll = () => {
		doc.scrollingElement.scrollTo(
			0,
			end == 'top' ? 25 : 475
		);
		end = end == 'top' ? 'bottom' : 'top';
	}

	document.querySelector('#click').addEventListener('click', click);
	document.querySelector('#scroll').addEventListener('click', scroll);
})();
